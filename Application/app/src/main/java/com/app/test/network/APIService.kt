package com.app.test.network

import com.app.test.model.DeliveryResponse
import retrofit2.http.GET
import rx.Observable
import java.util.*

interface APIService {

    companion object {

        val BASE_URL = "https://mock-api-mobile.dev.lalamove.com/"
    }

    @GET("deliveries")
    fun getDeliveries(): Observable<List<DeliveryResponse>>

}