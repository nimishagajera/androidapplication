package com.app.test.model

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.app.test.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


data class DeliveryResponse(
    val id: Int,
    val description: String,
    val imageUrl: String,
    val location: Location
)

data class Location(
    val lat: Double,
    val lng: Double,
    val address: String
)
/**
 *
 * @param imageView  The Imageview
 * @param imageUrl   The imageUrl from api response
 */
@BindingAdapter("imageUrl")
fun loadImage(imageView: ImageView, imageUrl: String){
    Glide.with(imageView.context)
            .load(imageUrl)
            .apply(RequestOptions().circleCrop().placeholder(R.mipmap.ic_launcher_round))
            .into(imageView)
}