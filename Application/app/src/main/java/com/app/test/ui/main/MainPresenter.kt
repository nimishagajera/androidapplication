package com.app.test.ui.main

import android.util.Log
import com.app.test.model.DeliveryResponse
import com.app.test.ui.base.BasePresenter
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class MainPresenter(private val view: MainContract.View):BasePresenter(view), MainContract.Presenter {

    override fun callAPIForDeliveryList() {
        view.showLoading()
        mSubscription.add( apiService.getDeliveries()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe({
                    deliveryListResponse: List<DeliveryResponse> ->
                    view.hideLoading()
                    Log.d("TAG", deliveryListResponse.toString())
                    view.onDeliveryResponseSuccess(deliveryListResponse)

                },
                        { // on Fail
                            e: Throwable ->
                            Log.d("TAG", e.toString())
                            view.hideLoading()
                            e.message?.let { view.onAPICallFailed(it) }
                        },
                        {
                        })
        )
    }
}