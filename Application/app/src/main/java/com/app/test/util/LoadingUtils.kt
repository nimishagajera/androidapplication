package com.app.test.util

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import android.widget.ProgressBar

object LoadingUtils {
    private var dialog: Dialog? = null

    fun showLoading(activity: Context) {
        try {
            dialog = Dialog(activity)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.setCancelable(false)

            val sp = ProgressBar(activity)
            dialog!!.setContentView(sp)
            dialog!!.window!!.setGravity(Gravity.CENTER)

            val lp = WindowManager.LayoutParams()
            lp.copyFrom(dialog!!.window!!.attributes)
            dialog!!.window!!.attributes = lp

            val draw = ColorDrawable(Color.TRANSPARENT)
            dialog!!.window!!.setBackgroundDrawable(draw)
            dialog!!.show()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun hideLoading() {
        try {
            if (dialog != null) {
                dialog!!.dismiss()
                dialog = null
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}