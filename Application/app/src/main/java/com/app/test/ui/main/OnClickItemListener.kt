package com.app.test.ui.main

import com.app.test.model.DeliveryResponse

interface OnClickItemListener {
    fun onClickItemListener(deliveryResponse: DeliveryResponse)
}