package com.app.test.ui.base

import com.app.test.app.AppController
import com.app.test.network.APIService
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject

open class BasePresenter(internal var mView: BaseContract.BaseView) : BaseContract.BasePresenter {

    protected lateinit var mSubscription: CompositeSubscription

    @Inject
    lateinit var apiService: APIService

    init {
        AppController.appComponent!!.inject(this)
        subscribe()
    }

    override fun subscribe() {
        mSubscription = CompositeSubscription()
    }

    override fun unSubscribe() {
        if (mSubscription != null)
            mSubscription.clear()

    }
}