package com.app.test.util

object Constant {

    var deliveryName: String = "description"
    var address: String = "address"
    var image: String = "imageUrl"
    var latitude: String = "latitude"
    var longitude: String = "longitude"

}