package com.app.test.ui.main

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.test.R
import com.app.test.databinding.LayoutItemDeliveryBinding
import com.app.test.model.DeliveryResponse

class DeliveryAdapter(private val context: Context,
                      private val deliveryList: List<DeliveryResponse>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var listener:OnClickItemListener

    fun setOnItemClickListener(listener: OnClickItemListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_item_delivery, parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = deliveryList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        val delivery = deliveryList[position]
        val viewHolder = holder as ViewHolder

        viewHolder.binding?.delivery = delivery
        viewHolder.binding?.executePendingBindings()

        viewHolder.binding?.cardView?.setOnClickListener {
            listener.onClickItemListener(delivery)
        }
    }

    inner class ViewHolder(view:View): RecyclerView.ViewHolder(view) {
        var binding: LayoutItemDeliveryBinding? = DataBindingUtil.bind(view)
    }
}