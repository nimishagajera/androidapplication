package com.app.test.app

import android.app.Application
import com.app.test.app.di.AppComponent
import com.app.test.app.di.module.ContextModule
import com.app.test.app.di.DaggerAppComponent
import com.app.test.app.di.module.NetworkModule

class AppController: Application() {

    companion object {
        var appComponent: AppComponent? = null
    }

    override fun onCreate() {
        super.onCreate()

        initComponent()
    }

    /**
     * appcomponent initialization for Dependency injection
     */
    private fun initComponent() {
        appComponent = DaggerAppComponent.builder()
                .contextModule(ContextModule(applicationContext))
                .networkModule(NetworkModule())
                .build()
    }
}