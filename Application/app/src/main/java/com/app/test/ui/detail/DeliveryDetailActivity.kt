package com.app.test.ui.detail

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.app.test.R
import com.app.test.databinding.ActivityDeliveryDetailBinding
import com.app.test.databinding.ActivityMainBinding
import com.app.test.util.Constant
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class DeliveryDetailActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityDeliveryDetailBinding
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_delivery_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        if (intent != null) {
            binding.txtDeliveryName.text = intent.getStringExtra(Constant.deliveryName)
            binding.txtDeliveryAddress.text = intent.getStringExtra(Constant.address)
            latitude = intent.getDoubleExtra(Constant.latitude, 0.0)
            longitude = intent.getDoubleExtra(Constant.longitude, 0.0)

            Glide.with(this)
                    .load(intent.getStringExtra(Constant.image))
                    .apply(RequestOptions().circleCrop().placeholder(R.mipmap.ic_launcher_round))
                    .into(binding.imgDelivery)
        }

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val location = LatLng(latitude, longitude)
        mMap.addMarker(MarkerOptions().position(location).title(intent.getStringExtra(Constant.address)))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location))
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 14.0f))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
