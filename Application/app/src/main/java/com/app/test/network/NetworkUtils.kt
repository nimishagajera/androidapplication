package com.app.test.network

import android.content.Context
import android.net.ConnectivityManager
import javax.inject.Inject
import retrofit2.HttpException

class NetworkUtils(private val context: Context) {

    @Inject
    lateinit var apiService: APIService

    companion object {
        /**
         *
         * @param context
         */
        fun isNetworkAvailable(context: Context): Boolean {

            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

        fun isHttpStatusCode(throwable: Throwable, statusCode: Int): Boolean {
            return throwable is HttpException && throwable.code() == statusCode
        }
    }
}