package com.app.test.network


import android.content.Context
import com.app.test.R
import java.io.IOException
import java.net.SocketException

class GenericException(private var context: Context, e: Exception) : IOException() {

    private var errorMessage: String

    init {
        errorMessage = context.getString(R.string.str_internal_server_error)

        if (e is SocketException)
            errorMessage = context.getString(R.string.str_internet_connection_error)
    }

    override val message: String?
        get() = errorMessage

}
