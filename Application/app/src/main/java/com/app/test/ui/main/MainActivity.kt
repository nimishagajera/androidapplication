package com.app.test.ui.main

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.app.test.R
import com.app.test.databinding.ActivityMainBinding
import com.app.test.model.DeliveryResponse
import com.app.test.ui.detail.DeliveryDetailActivity
import com.app.test.util.Constant
import com.app.test.util.LoadingUtils


class MainActivity : AppCompatActivity(), MainContract.View {

    private lateinit var binding: ActivityMainBinding
    private lateinit var presenter: MainContract.Presenter
    private lateinit var adapter: DeliveryAdapter
    private lateinit var deliverList: ArrayList<DeliveryResponse>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        presenter = MainPresenter(this)
        presenter.callAPIForDeliveryList()

        deliverList = ArrayList()

        adapter = DeliveryAdapter(this,deliverList)
        binding.recyclerDelivery.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        binding.recyclerDelivery.addItemDecoration(DividerItemDecoration(this, 0))
        binding.recyclerDelivery.adapter = adapter

        adapter.setOnItemClickListener(object: OnClickItemListener {
            override fun onClickItemListener(deliveryResponse: DeliveryResponse) {
                Log.d("deliveryResponse","deliveryResponse $deliveryResponse")
                val intent = Intent(this@MainActivity, DeliveryDetailActivity::class.java)
                intent.putExtra(Constant.deliveryName, deliveryResponse.description)
                intent.putExtra(Constant.image, deliveryResponse.imageUrl)
                intent.putExtra(Constant.address, deliveryResponse.location.address)
                intent.putExtra(Constant.latitude, deliveryResponse.location.lat)
                intent.putExtra(Constant.longitude, deliveryResponse.location.lng)
                startActivity(intent)
            }
        })
    }

    override fun showLoading() {
        LoadingUtils.showLoading(this)
    }

    override fun hideLoading() {
        LoadingUtils.hideLoading()
    }

    override fun onDeliveryResponseSuccess(deliveryListResponse: List<DeliveryResponse>) {
        binding.txtError.visibility = View.GONE
        binding.recyclerDelivery.visibility = View.VISIBLE
        deliverList.addAll(deliveryListResponse)
        adapter.notifyDataSetChanged()
    }

    override fun onAPICallFailed(message: String) {
        Log.d("TAG-ERROR", message)
        binding.txtError.visibility = View.VISIBLE
        binding.recyclerDelivery.visibility = View.GONE

        val snackbar = Snackbar
                .make(binding.root, R.string.str_error, Snackbar.LENGTH_LONG)
                .setAction(R.string.retry) {
                    presenter.callAPIForDeliveryList()
                }

        snackbar.show()
    }
}
