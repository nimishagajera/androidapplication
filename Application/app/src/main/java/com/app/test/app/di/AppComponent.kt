package com.app.test.app.di

import com.app.test.app.di.module.ContextModule
import com.app.test.app.di.module.NetworkModule
import com.app.test.ui.base.BasePresenter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ContextModule::class, NetworkModule::class))
interface AppComponent {

    fun inject(presenter: BasePresenter)
}