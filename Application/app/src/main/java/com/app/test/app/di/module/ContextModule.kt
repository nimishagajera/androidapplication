package com.app.test.app.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ContextModule(@get: Singleton
                    @get: Provides
                    val context: Context)