package com.app.test.ui.main

import com.app.test.model.DeliveryResponse
import com.app.test.ui.base.BaseContract

interface MainContract {

    interface View: BaseContract.BaseView{

        fun onDeliveryResponseSuccess(deliveryListResponse: List<DeliveryResponse>)

        fun onAPICallFailed(message: String)
    }

    interface Presenter: BaseContract.BasePresenter {

        fun callAPIForDeliveryList()

    }
}